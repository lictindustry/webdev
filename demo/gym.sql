-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 25, 2017 at 12:26 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gym`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth`
--

CREATE TABLE `auth` (
  `id` int(11) NOT NULL,
  `uname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `token` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth`
--

INSERT INTO `auth` (`id`, `uname`, `email`, `password`, `role`, `status`, `token`) VALUES
(6, 'pawan', 'pawan@pawan.com', 'pawan', 'regular', 1, ''),
(7, 'admin', 'admin@admin.com', 'admin', 'admin', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `mname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `area` varchar(255) NOT NULL,
  `telephone` varchar(12) NOT NULL,
  `telephone2` varchar(12) NOT NULL,
  `created` date DEFAULT NULL,
  `expired` tinyint(4) NOT NULL,
  `image` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `fname`, `mname`, `lname`, `gender`, `address`, `area`, `telephone`, `telephone2`, `created`, `expired`, `image`) VALUES
(49, 'Maung', 'Then ', 'Line', 'M', 'Tekpara', 'coxbazar', '01784333535', '', '2017-06-22', 1, 'upload/maung.png'),
(50, 'Khin', 'Khin', 'U', 'F', 'Tekpara', 'Cox''sBazar', '01784333535', '', '2017-06-25', 1, 'upload/zecurbag.jpg'),
(51, 'Usen ', 'Hen', 'Rakhine', 'M', 'Tekpara', 'coxbazar', '01784333535', '', '2017-06-21', 1, 'upload/maung (1).png'),
(52, 'testCox5', 'testCox5', 'testCox5', 'M', 'testCox5', 'testCox5', '01784333535', '', '2017-06-07', 1, 'upload/bubu.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `member_plan`
--

CREATE TABLE `member_plan` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `package_type` varchar(255) NOT NULL,
  `package_period` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `expire_date` date DEFAULT NULL,
  `paid` int(11) NOT NULL,
  `unpaid` int(11) NOT NULL DEFAULT '0',
  `next_installment` date DEFAULT NULL,
  `desc` text NOT NULL,
  `created` date DEFAULT NULL,
  `modified` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member_plan`
--

INSERT INTO `member_plan` (`id`, `member_id`, `package_type`, `package_period`, `start_date`, `expire_date`, `paid`, `unpaid`, `next_installment`, `desc`, `created`, `modified`) VALUES
(43, 49, '0', '0', '2017-06-22', '0000-00-00', 5000, 0, '0000-00-00', '', '2017-06-25', '2017-06-25'),
(44, 50, '0', '0', '2017-06-25', '0000-00-00', 1000, 0, '0000-00-00', '', '2017-06-25', '2017-06-25'),
(45, 51, '0', '0', '2017-06-21', '0000-00-00', 1200, 0, '0000-00-00', '', '2017-06-25', '2017-06-25'),
(46, 52, '0', '0', '2017-06-07', '0000-00-00', 8000, 0, '0000-00-00', '', '2017-06-25', '2017-06-25');

-- --------------------------------------------------------

--
-- Table structure for table `plan`
--

CREATE TABLE `plan` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `plan`
--

INSERT INTO `plan` (`id`, `name`, `code`) VALUES
(3, 'Cardio Training', 'CT'),
(4, 'Weight Training  plus Cardio Training  ', 'WCT'),
(8, 'Weight Training', 'WT'),
(9, 'group entry', 'GE');

-- --------------------------------------------------------

--
-- Table structure for table `temp`
--

CREATE TABLE `temp` (
  `id` int(11) NOT NULL,
  `flag` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp`
--

INSERT INTO `temp` (`id`, `flag`) VALUES
(1, 'yes'),
(2, 'yes'),
(3, 'yes'),
(4, 'yes'),
(5, 'no'),
(6, 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `teriff`
--

CREATE TABLE `teriff` (
  `id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `offer` varchar(255) NOT NULL,
  `notes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teriff`
--

INSERT INTO `teriff` (`id`, `plan_id`, `duration`, `price`, `offer`, `notes`) VALUES
(4, 8, '3  months', 900, 'none', 'Weight training for 3 months 800 fee plus 100 entry free total 900'),
(5, 8, '6 months', 1600, '1 month free', 'Weight training for 6 months 1500 fee plus 100 entry free total 1600 plus one month free total 7 month'),
(6, 8, '12 months', 3000, '2 months free', 'Weight training for 12 months 3000 fee plus 2 month free total 14 months'),
(7, 3, '3  months', 1200, 'none', '1200 rupees for 3 months'),
(8, 3, '6 months', 2000, '', '2000 rupees for 6 months'),
(9, 3, '12 months', 4000, '', '4000 rupees for 12 months'),
(10, 9, '3  months', 2550, '', '3 members ! per member 750 plus 100 entry fee (total 850).\r\ntotal 2550'),
(11, 9, '3  months', 4000, '', '5 members ! per member 700 plus 100 entry fee (total 800). total 4000');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth`
--
ALTER TABLE `auth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member_plan`
--
ALTER TABLE `member_plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plan`
--
ALTER TABLE `plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp`
--
ALTER TABLE `temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teriff`
--
ALTER TABLE `teriff`
  ADD PRIMARY KEY (`id`),
  ADD KEY `plan_id` (`plan_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth`
--
ALTER TABLE `auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `member_plan`
--
ALTER TABLE `member_plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `plan`
--
ALTER TABLE `plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `temp`
--
ALTER TABLE `temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `teriff`
--
ALTER TABLE `teriff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `teriff`
--
ALTER TABLE `teriff`
  ADD CONSTRAINT `teriff_ibfk_1` FOREIGN KEY (`plan_id`) REFERENCES `plan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
